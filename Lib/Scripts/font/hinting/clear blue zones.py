# [h] clear blue zones

from hTools2.modules.messages import no_font_open

f = CurrentFont()

if f is not None:
    f.info.postscriptBlueValues = []
    f.info.postscriptOtherBlues = []
    f.info.postscriptFamilyBlues = []
    f.info.postscriptFamilyOtherBlues = []

else:
    print no_font_open
