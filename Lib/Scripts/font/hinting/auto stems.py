# [h] auto PS stems

from hTools2.modules.pshinting import set_stems
from hTools2.modules.messages import no_font_open

f = CurrentFont()

if f is not None:
    set_stems(f)

else:
    print no_font_open
