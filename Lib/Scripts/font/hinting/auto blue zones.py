# [h] auto blue zones

from hTools2.modules.messages import no_font_open
from hTools2.modules.pshinting import set_bluezones

f = CurrentFont()

if f is not None:
    set_bluezones(f)

else:
    print no_font_open
