# [h] clear PS stems

from hTools2.modules.messages import no_font_open

f = CurrentFont()

if f is not None:
    f.info.postscriptStemSnapV = []
    f.info.postscriptStemSnapH = []

else:
    print no_font_open
