# [h] center selected glyphs

from hTools2.modules.messages import no_glyph_selected, no_font_open

f = CurrentFont()

if f is not None:

    glyph_names = f.selection

    if len(glyph_names) > 0:
        for glyph_name in glyph_names:
            g = f[glyph_name]
            g.prepareUndo('center glyphs')
            w = g.width
            g.leftMargin = (g.leftMargin + g.rightMargin) * 0.5
            g.width = w
            g.performUndo()

    # no glyph selected
    else:
        print no_glyph_selected

# no font open
else:
    print no_font_open
